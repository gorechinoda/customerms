package com.example.customerms.service;

import com.example.customerms.exception.CustomerNotFoundException;
import com.example.customerms.model.Customer;
import com.example.customerms.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public Customer createCustomer(Customer customer) {
        customer.setId(null);
        customer.setCreatedBy("System");
        customer.setCreatedOn(new Date());
        return this.customerRepository.save(customer);
    }

    public Customer updateCustomer(Customer customer, long id) {
        Customer customerToSave = this.customerRepository.findById(id).orElse(null);
        if (customerToSave == null) {
            return this.createCustomer(customer);
        }
        customer.setId(id);
        customer.setUpdatedBy("Milton");
        customer.setUpdatedOn(new Date());
        return this.customerRepository.save(customer);
    }

    public Customer getOneCustomer(long id) {
        return this.customerRepository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
    }

    public void deleteCustomer(long id) {
        Customer customer = this.getOneCustomer(id);
        this.customerRepository.delete(customer);
    }

    public List<Customer> getAllCustomers() {
        return this.customerRepository.findAll();
    }
}
