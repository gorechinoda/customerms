package com.example.customerms.controller;

import com.example.customerms.dto.CustomerDTO;
import com.example.customerms.model.Customer;
import com.example.customerms.service.CustomerService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ModelMapper modelMapper;

    Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @PostMapping()
    public EntityModel<CustomerDTO> addCustomer(@Valid @RequestBody CustomerDTO customerDTO) {
        Customer customer = this.modelMapper.map(customerDTO, Customer.class);
        customer = this.customerService.createCustomer(customer);
        return this.convertToEntityModel(customer);
    }

    @PutMapping("/{id}")
    public EntityModel<CustomerDTO> updateCustomer(@Valid @RequestBody CustomerDTO customerDTO, @PathVariable long id) {
        Customer customer = this.modelMapper.map(customerDTO, Customer.class);
        customer = this.customerService.updateCustomer(customer, id);
        return this.convertToEntityModel(customer);
    }

    @GetMapping()
    public ResponseEntity<List<CustomerDTO>> getAllCustomers() {
        try {
            List<Customer> customerList = this.customerService.getAllCustomers();
            List<CustomerDTO> customerDTOList = new ArrayList<>();
            for(Customer customer: customerList){
                CustomerDTO customerDTO = convertToEntityModel(customer);
                customerDTOList.add(customerDTO);
            }
//            CollectionModel<CustomerDTO> result = CollectionModel.of(customerDTOList, linkTo(methodOn(CustomerController.class).getAllCustomers()).withSelfRel());
            return ResponseEntity.ok(customerDTOList);
        }catch(Exception ex){
            this.logger.error(ex.getMessage(),ex);
            return ResponseEntity.internalServerError().body(null);
        }

    }

    @GetMapping("/{id}")
    public EntityModel<CustomerDTO> getOneCustomer(@PathVariable long id) {
        return this.convertToEntityModel(this.customerService.getOneCustomer(id));
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable long id) {
        this.customerService.deleteCustomer(id);
    }

    @GetMapping("/audit")
    public List<Customer> getAllCustomersWithAuditData() {
        return this.customerService.getAllCustomers();
    }

    @GetMapping("/audit/{id}")
    public Customer getOneCustomerWithAuditData(@PathVariable long id) {
        return this.customerService.getOneCustomer(id);
    }

    private CustomerDTO convertToEntityModel(Customer customer) {
        CustomerDTO customerDTO = this.modelMapper.map(customer, CustomerDTO.class);
        customerDTO.add(linkTo(methodOn(CustomerController.class).getOneCustomer(customerDTO.getId())).withSelfRel(),
                linkTo(methodOn(CustomerController.class).getAllCustomers()).withRel("allCustomers"),
                linkTo(methodOn(CustomerController.class).getOneCustomerWithAuditData(customerDTO.getId())).withRel("customerAudit"),
                linkTo(methodOn(CustomerController.class).getAllCustomersWithAuditData()).withRel("allCustomersAudit"));

        return customerDTO;
    }


}
