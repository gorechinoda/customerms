package com.example.customerms.dto;

import lombok.Data;
import org.springframework.hateoas.EntityModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class CustomerDTO  extends EntityModel {

    private long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotNull
    private Date dateOFBirth;
}
