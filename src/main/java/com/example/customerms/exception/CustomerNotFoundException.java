package com.example.customerms.exception;

public class CustomerNotFoundException extends RuntimeException {
    public CustomerNotFoundException(long id) {
        super("Customer " + id + " not Found");
    }
}
